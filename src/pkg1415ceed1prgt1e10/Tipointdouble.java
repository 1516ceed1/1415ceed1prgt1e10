/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt1e10;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Tipointdouble {

  public static void main(String args[]) {
    int a = 10; // variable entera
    double b = 10.0; // variable punto flotante
    System.out.println("a= " + a);
    System.out.println("b= " + b);
    a = a / 4;
    b = b / 4;
    System.out.println("a/4= " + a);
    System.out.println("b/4= " + b);
  }
}
/* EJECUCION
a= 10
b= 10.0
a/4= 2
b/4= 2.5
*/
