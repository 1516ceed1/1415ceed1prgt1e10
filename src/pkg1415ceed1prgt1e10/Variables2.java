/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt1e10;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Variables2 {

  static int m = 5;

  public static void f1() {
    int l;
    l = m + 2;
    System.out.println(" Suma : " + l);
  }

  public static void f2() {
    int l;
    l = m + 1;
    System.out.println(" Suma : " + l);
  }

  public static void main(String[] args) {
    Variables2 a = new Variables2();
    a.f1();
    a.f2();
  }
}
/* Ejecucion :
Suma : 7
Suma : 6
*/
