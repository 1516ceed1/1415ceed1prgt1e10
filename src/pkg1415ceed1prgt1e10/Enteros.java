/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt1e10;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Enteros {

  public static void main(String arg[]) {
    byte midato1 = 1;
    short midato2 = 100;
    int midato3 = 10000;
    long midato4 = 100000000;
    System.out.println("midato = " + midato1);
    System.out.println("midato = " + midato2);
    System.out.println("midato = " + midato3);
    System.out.println("midato = " + midato4);
  }
}
