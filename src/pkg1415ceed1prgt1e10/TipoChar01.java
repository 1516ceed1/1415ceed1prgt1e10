/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1415ceed1prgt1e10;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class TipoChar01 {

  public static void main(String[] arg) {
    char c = 'A';
    int n;
    n = c + 1;
    System.out.println("n: " + n);
    n = 'A' + 1;
    c = (char) (n);
    System.out.println("c: " + c);
  }
}
/* EJECUCION
 n: 66
 c: B
 */
